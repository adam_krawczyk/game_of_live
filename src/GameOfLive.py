#!/usr/bin/env python3

import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import json

x = '{{"val": 0}, {"val": 0}, {"val": 0}, {"val": 0}, {"val": 1}, {"val": 1}, {"val": 1}, {"val": 0}, {"val": 0}, {"val": 0},{"val": 0}, {"val": 0}, {"val": 0}, {"val": 0}, {"val": 1},{"val": 1}, {"val": 1}, {"val": 0}, {"val": 0},{"val": 1}, {"val": 1}, {"val": 0}, {"val": 0}}'

l0 = '{"val0": 0,"val1": 1, "val2":0}'
l1 = '{"val0": 0,"val1": 1, "val2":1}'
l2 = '{"val0": 1,"val1": 1, "val2":0}'
y = json.dumps(l0)
print(y)


class GameOfLive:
    def __init__(self):
        super().__init__()
        print("initialized")
        self.ON = 255
        self.OFF = 0
        self.vals = [self.ON, self.OFF]

    def jsonFigure(self, name, N, grid):
        pass

    def showFigure(self, i, j, grid):
        """adds a glider with top left cell at (i, j)"""
        glider = np.array([[self.OFF,    self.OFF, self.ON],
                           [self.ON,  self.OFF, self.ON],
                           [self.OFF,  self.ON, self.ON]])

        glider2 = np.array([[self.OFF,    self.OFF, self.ON],
                            [self.ON,  self.OFF, self.ON],
                            [self.OFF,  self.ON, self.ON]])

        grid[i:i+3, j:j+3] = glider
        grid[i+5:i+8, j:j+3] = glider2

    def randomGrid(self, N):
        """returns a grid of NxN random values"""
        return np.random.choice(self.vals, N*N, p=[0.2, 0.8]).reshape(N, N)

    def update(self, frameNum, img, grid, N):

        # copy grid since we require 8 neighbors
        # for calculation and we go line by line
        newGrid = grid.copy()
        for i in range(N):
            for j in range(N):

                # compute 8-neghbor sum
                # using toroidal boundary conditions - x and y wrap around
                # so that the simulaton takes place self.ON a toroidal surface.
                total = int((grid[i, (j-1) % N] + grid[i, (j+1) % N] +
                             grid[(i-1) % N, j] + grid[(i+1) % N, j] +
                             grid[(i-1) % N, (j-1) % N] + grid[(i-1) % N, (j+1) % N] +
                             grid[(i+1) % N, (j-1) % N] + grid[(i+1) % N, (j+1) % N])/255)

                # apply Conway's rules
                if grid[i, j] == self.ON:
                    if (total < 2) or (total > 3):
                        newGrid[i, j] = self.OFF
                else:
                    if total == 3:
                        newGrid[i, j] = self.ON

        # update data
        img.set_data(newGrid)
        grid[:] = newGrid[:]
        return img


# main() function
def main():

    gol = GameOfLive()

    # Command line args are in sys.argv[1], sys.argv[2] ..
    # sys.argv[0] is the script name itself and can be ignored
    # parse arguments
    parser = argparse.ArgumentParser(
        description = "Conway's Game of Life for GSoC.")

    # add arguments
    parser.add_argument('--grid-size', dest = 'N', required = False)
    parser.add_argument('--json', dest = 'json', required = False)

    args=parser.parse_args()

    # set grid size
    N=50
    if args.N and int(args.N) > 8:
        N=int(args.N)

    # set animation update interval
    # updateInterval = 50
    # if args.interval:
    #     updateInterval = int(args.interval)

    # declare grid
    grid=np.array([])
    grid=np.zeros(N*N).reshape(N, N)
   # grid = gol.randomGrid(N) #np.zeros(N*N).reshape(N, N)
    gol.showFigure(0, 0, grid)

    # set up animation
    fig, ax=plt.subplots()
    img=ax.imshow(grid, interpolation = 'nearest')

    ani=animation.FuncAnimation(fig, gol.update, fargs = (img, grid, N, ),
                                  frames = 10,
                                  interval = 100,
                                  save_count = 50)

    plt.show()


# call main
if __name__ == '__main__':
    main()
